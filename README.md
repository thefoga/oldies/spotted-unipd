# Spotted Unipd
*Figure out how you can find your sweet half or how to get attention and get liked in one of the most popular @Unipd facebook pages. See [the offical facebook page](https://www.facebook.com/471034402958762) for details.*
> This project is no longer under active development

[![PyPI version](https://badge.fury.io/py/pyspottedunipd.svg)](https://pypi.python.org/pypi/pyspottedunipd) [![Code Health](https://landscape.io/github/sirfoga/spotted-unipd/master/landscape.svg?style=flat
)](https://landscape.io/github/sirfoga/spotted-unipd/master)

There is a [detailed blog post](https://sirfoga.github.io/2017/04/19/pyspottedunipd) explaining all this fuss in a nice and easy way. Please, take a look.


## Install
```
$ pip3 install . --upgrade --force-reinstall
```
*To install from source, recommended.*


## Usage
A simple `pyspottedunipd -h` from the terminal should result in this output
```
usage: -t <access token> -f <format of output file [json, csv]> -o <path to output file>

optional arguments:
  -h, --help         show this help message and exit
  -t TOKEN           access token to use Facebook API
  -m MIN_POST_COUNT  min number of posts to fetch
  -f FORMAT_OUT      format of output file [json, csv]
  -o PATH_OUT        path to output file
```
When called with appropriate args, `pyspottedunipd` prints to stdout some debug info to get the idea of what the bot is doing in that moment:
```
Parsing 304-th Facebook post 471034402958762_1478146872247505
	Parsing post Biblioteca Metel ( 471034402958762_1478146872247505 ) at 15:09:12 on 2017-03-30 1.0 ***   1.0 -.-   0 |-
Parsing 305-th Facebook post 471034402958762_1478146342247558
	Parsing post Eri seduta nei t ( 471034402958762_1478146342247558 ) at 15:08:23 on 2017-03-30 0.0 ***   0.0 -.-   0 |-
Parsing 306-th Facebook post 471034402958762_1477971428931716
	Parsing post Ciao, spotto il  ( 471034402958762_1477971428931716 ) at 11:02:24 on 2017-03-30 0.0 ***   2.0 -.-   0 |-
```
When it's done, the bot saves `.csv` or `.json` posts dumps of the [Spotted Unipd facebook page](https://www.facebook.com/471034402958762). The files will look like
```
id,time,date,comments,message,shares,likes
471034402958762_1499980280064164,12:44:02,2017-04-18,0.0,"Spotto il ragazzo alto e moro che ha preso il treno a Mestre direzione Bologna alle 13 e 54. Sceso a Padova. Jeans larghi, vans, felpa grigia col cappuccio e giacca verde. Skateboard con la parte inferiore disegnata con colori accesi, fucsia in particolare. A presto, spero.",0,0.0
471034402958762_1499979956730863,12:43:35,2017-04-18,0.0,"Spotto il ragazzo che era seduto di fronte a me sul treno partito da Verona alle 10.20 e diretto a Venezia S. Lucia il 18/04. Avevi una giacca verde, i jeans e una maglia color panna. Stavi studiando qualcosa riguardante le equazioni mentre io stavo leggendo i miei appunti di letteratura francese e siamo scesi entrambi a Padova. Non sono riuscita a parlarti perché due turisti tedeschi si sono seduti accanto a noi, ma mi hanno tanto colpita i tuoi occhi azzurri. Mi piacerebbe rivederti e conoscerti meglio!",0,0.0
471034402958762_1499932023402323,11:42:30,2017-04-18,0.0,Spottiamo il culo di marmo e i polpacci d'acciaio del ragazzo che sfrecciava in bici alle 12:45 alla rotonda del biri... abbiamo apprezzato la scelta dei bermuda in questa giornata fredda e ventosa che hai riscaldato con la tua prepotente virilità. La lobby gay del pandino bianco (ci sono comunque anche due donne all'interno),0,9.0
```
Browse a [`sample csv output`](data/csv/sample.csv) for about 8 days (100 posts).

## Machine learning analysis
As of now, the [analysis](pyspottedunipd/analysis/cli.py) has not been included in the main cli program, nor has a mature command line parser: you can play with it as you want!
There is lots of machine-learning stuff already done, and you can browse some samples [here](data/images). Mainly the focus is on clustering, best features selection and regression. Feel free to [contribute](https://github.com/sirfoga/pygce/pulls)!

![3d clusters](data/images/5_clusters.png "3d clusters")
![3 clusters](data/images/3_clusters.png "3 clusters")
![correlation matrix](data/images/correlation.png "correlation matrix")
![fitting likes count](data/images/lin_fit_likes.png "fitting likes count")


## Statistical analysis
It comes not as a surprise to see that the most liked posts are in March, April and May: the springs starts in March, while in April and May the usual 'navigli' parti es are in town every evening. As usual during summer (when there are lots of exams) there is no much activity.

![likes count per month](data/images/number_of_likes_per_month.png "likes count per month")
![likes count per hour](data/images/number_of_likes_per_hour.png "likes count per hour")

## Text analysis
Looking more at the *content* of the posts we can see that there is a very detailed trend towards spotting females (rather than males) and the places where people are usually *spotted* are the classroom, study room, bars and trains. Here are the top 30 words that appear in the posts:
```
<word>      <count>
spotto 		3094
ragazza 	1793
ragazzo 	1318
palesati 	1194
sono 		1031
capelli 	924
oggi 		914
occhi 		722
vorrei 		635
aula 		599
spottare 	569
come 		561
anche 		532
padova 		521
caffè 		501
studio 		488
siamo 		486
amica 		473
verso 		462
molto 		416
treno 		415
sera 		415
avevi 		414
ieri 		404
occhiali 	396
spritz 		390
bionda 		386
fatto 		374
solo 		371
davanti 	370
```
whereas here is the chart with the top words per likes percentile (i.e the 99-percentile has the posts which have the 99-percentile likes): note how many words gain/lose positions based on the post likes percentile.
![top words per percentile](data/images/top_words_per_percentile.png "top words per percentile")

## N-grams
As of now there is no public ngram analyzer for this data (there are some tweaks to be done ...) but for the sake of machine-learning here are some samples you can generate:
```
spotto alessandra che venerd 16 12 stava mangiando la pizza da sola nella mensa del piovego con un vassoio pieno di caff palesati che te ne offro io una la prossima volta ti offro qualcosa da bere per ringraziarti
```

```
spotto te che oggi 16 02 16 stavano studiando in aula studio al vallisneri castana con maglioncino marrone e bianco ogni tanto mi torni in mente come se la conversazione non fosse finita jappelli 05 02
```

```
spotto il culo ho risposto imitando il loro stesso gesto non avevo proprio capito scusatemi ci sono arrivato dopo quando una macchina mi ha abbagliato
```

```
spotto minutes ago we've both saw some lovely ladies who just left their homes near to the house of gallileo gallilei. honestly we are still thinking of you accidentally almost naked in her room and we think that we want your tegas. oggi, martedì 12 nel pomeriggio. capelli biondi, altezza media, bel 
```

```
spotto per conto di una mia amica spottiamo il ragazzo moro con la barba e gli occhiali che oggi pomeriggio era in metelli maglia a righe bianche e azzurre che penso venga da montagnana ti prego posso farmi avanti
```

```
spotto un libro seduto su una panchina stavi mangiando dei mandarini avevi una camicia bordeaux e un paio di jeans neri la mia amica ti sta pensando da tutta oggi palesati
```

```
tu e la mia amica spottiamo il ragazzo che oggi pomeriggio era in aula studio al vallisneri con pantaloni neri e un paio
```

## Documentation
- You can browse the [html](docs/doxygen/html/index.html) (or if you prefer the [epydoc docs](docs/epydoc/html/index.html))
- There is also the [pdf](docs/doxygen/latex/refman.pdf) version (for the epydoc pdfs go [here](docs/epydoc/pdf/api.pdf))


## Questions and issues
The [github issue tracker](https://github.com/sirfoga/spotted-unipd/issues) is **only** for bug reports and feature requests. Anything else, such as questions for help in using the library, should be posted as [pull requests](https://github.com/sirfoga/spotted-unipd/pulls).


## License
[Apache License](http://www.apache.org/licenses/LICENSE-2.0) Version 2.0, January 2004
