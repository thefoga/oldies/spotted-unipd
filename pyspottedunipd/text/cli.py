# !/usr/bin/python3
# coding: utf-8

# Copyright 2017 Stefano Fogarollo
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import argparse
import os
import re
from collections import Counter

WORDS_BLACKLIST_FIlE = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                    "words_blacklist.txt")  # file with blacklisted words


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage="-i <path to input file>")
    parser.add_argument("-i", dest="path_in", help="path to input file", required=True)

    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    return str(args.path_in)


def check_args(path_in):
    """
    :param path_in: str
        Path to input file
    :return: bool
        True iff args are correct
    """

    assert (os.path.exists(path_in))

    return True


def get_words(string, min_length=0, blacklist=[]):
    """
    :param string: str
        String
    :param min_length: int
        Minimum length of words to consider
    :param blacklist: [] of str
        List of blacklisted words (not to be considered)
    :return: [] of str
        List of words in string
    """

    words = re.findall(r'\w+', string)  # filter REGEX
    words = [str(w).strip() for w in words if len(str(w)) >= min_length]  # filter by length
    words = [w for w in words if (w not in blacklist)]  # remove blacklisted words
    return words


def get_words_in_file(path_in):
    """
    :param path_in: str
        Path to input file
    :return: [] of str
        List of words in file
    """

    with open(path_in, "r") as i:
        return get_words(i.read())


def get_words_count(words):
    """
    :param words: [] of str
        List of words to analyze
    :return: [] of str
        Dictionary <word, quantity> sorted by quantity
    """

    lower_words = [str(w).lower() for w in words]  # lower all the words
    return Counter(lower_words)  # counts the number each time a word appears


def get_top_words(words, n=10):
    """
    :param words: [] of str
        List of words to analyze
    :param n: int
        Top words to get
    :return: [] of str
        Top words in list
    """

    word_counts = get_words_count(words)
    return word_counts.most_common(n)


def get_top_words_in_file(path_in, blacklist_file, n=30):
    """
    :param path_in: str
        Path to input file
    :param blacklist_file:str
        Path to blacklist file
    :param n: int
        Top words to get
    :return: [] of str
        Top words in file
    """

    blacklisted_words = get_words(open(blacklist_file, "r").read(), min_length=0)  # get list of blacklisted
    words = get_words(open(path_in, "r").read(), min_length=4, blacklist=blacklisted_words)  # get valuable words
    return get_top_words(words, n=n)  # get top words


def main():
    path_in = parse_args(create_args())
    if check_args(path_in):
        top_words = get_top_words_in_file(path_in, WORDS_BLACKLIST_FIlE)
        for word, counter in top_words:
            print(str(word), str(counter))
    else:
        print("Error while parsing args.")


if __name__ == '__main__':
    main()
