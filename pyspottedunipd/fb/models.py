# !/usr/bin/python3
# coding: utf-8

# Copyright 2017 Stefano Fogarollo
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import datetime
import json

import facebook
import requests

from pyspottedunipd import utils


class FacebookGraphApi(object):
    """ The official Facebook graph API driver (parser, scraper ...) """

    def __init__(self, token):
        object.__init__(self)

        self.token = str(token)

    @staticmethod
    def get_api_url(version="2.8"):
        """
        :param version: str
            Version of API to use
        :return: str
            Url of graph API
        """

        return "https://graph.facebook.com/" + "v" + str(version) + "/"

    @staticmethod
    def parse_api_response(url):
        """
        :param url: str
            Url to fetch
        :return: json loaded
            Response json-parsed
        """

        response = requests.get(url)
        return json.loads(response.text)

    def get_api_driver(self, version="2.7"):
        """
        :param version: str
            Version of API to use
        :return: GraphAPI
            Facebook API driver
        """

        return facebook.GraphAPI(access_token=self.token, version=version)

    def get_page_posts_url(self, page_id):
        """
        :param page_id: str
            ID of page to scrape
        :return: str
            Url of posts of page
        """

        return self.get_api_url() + str(page_id) + "/posts?limit=100&access_token=" + self.token

    def get_post_summary_urls(self, post_id):
        """
        :param post_id: str
            ID if post
        :return: {} with keys 'likes', 'comments'
            Urls to get info about key
        """

        likes = self.get_api_url() + str(post_id) + "/likes?summary=1&access_token=" + self.token
        comments = self.get_api_url() + str(post_id) + "/comments?summary=1&access_token=" + self.token
        shares = self.get_api_url() + str(post_id) + "?fields=shares&summary=1&access_token=" + self.token
        return {
            "likes": likes,
            "comments": comments,
            "shares": shares
        }


class FacebookBot(object):
    """ A bot that scrapes Facebook with a authentication token """

    def __init__(self, token):
        """
        :param token: str
            Token to use Facebook API
        """

        object.__init__(self)
        self.token = str(token)
        self.api = FacebookGraphApi(self.token)
        self.graph = self.api.get_api_driver()

    def get_latest_posts_of_page(self, page_id, number_to_fetch):
        """
        :param page_id: str
            ID of page to scrape
        :param number_to_fetch: int
            Minimum number of posts to fetch
        :return: [] of str in json format
            Each dict is a raw post (straight out of the API)
        """

        raw_posts = []  # list of raw fetched posts
        url = self.api.get_page_posts_url(page_id)  # url to get API content
        while len(raw_posts) < number_to_fetch and url is not None:
            print("Fetched", len(raw_posts), "posts")  # debug info
            reponse = self.api.parse_api_response(url)  # read and parse raw response

            try:  # parse next url to get posts
                url = reponse["paging"]["next"]  # url to get next if number of fetched post is not enough
            except:
                url = None  # no more posts

            try:  # parse posts data
                data = reponse["data"]  # real data in response
            except:
                data = []  # no more posts

            raw_posts += data

        raw_posts = raw_posts[:number_to_fetch]  # take exact number
        posts = []  # list of parsed posts
        for raw_post in raw_posts:
            p = FacebookPost(raw_post)  # parse post
            print("Parsing", str(len(posts)) + "-th Facebook post", p.facebook_id)  # debug info
            p.get_totals(self.api)  # get likes and comments count
            posts.append(p)  # append to parsed list

        return posts

    def download_page_posts(self, page_id, min_number_to_fetch, output_file):
        """
        :param page_id: str
            ID of page to scrape
        :param min_number_to_fetch: int
            Minimum number of posts to fetch
        :param output_file: str
            Path to output file to write data
        :return: void
            Saves .csv file with posts data
        """

        posts = self.get_latest_posts_of_page(page_id, min_number_to_fetch)
        dicts = [p.to_dict() for p in posts]  # get dictionaries
        utils.save_dicts_to_csv(
            dicts,
            output_file
        )  # save to .csv


class FacebookPost(object):
    """ A standard Facebook post """

    def __init__(self, raw_json_api_response):
        """
        :param raw_json_api_response: str in json format
            Standard format coming out of the official API
        """

        object.__init__(self)
        self.raw_json = raw_json_api_response
        self.message = None
        self.time = None
        self.date = None
        self.facebook_id = None
        self.likes_count = None
        self.comments_count = None
        self.shares_count = None

        self.parse()  # parse raw json

    def parse(self):
        """
        :return: void
            Parses raw json response of API and set object's fields
        """

        try:
            self.message = str(self.raw_json["message"])
        except:
            self.message = str(self.raw_json["story"])  # in case of a picture

        raw_time = str(self.raw_json["created_time"]).split("+")[0]  # discard +0000 (CET)
        raw_time = datetime.datetime.strptime(raw_time, "%Y-%m-%dT%H:%M:%S")  # parse time to get datetime
        self.time = str(raw_time.time())
        self.date = str(raw_time.date())

        self.facebook_id = str(self.raw_json["id"])

    def get_totals(self, api_driver):
        """
        :param api_driver: FacebookGraphApi
            API driver to use
        :return: void
            Gets counts of likes and comments
        """

        urls = api_driver.get_post_summary_urls(self.facebook_id)  # get likes and comments urls
        raw_likes_data = api_driver.parse_api_response(urls["likes"])  # get raw data
        raw_comments_data = api_driver.parse_api_response(urls["comments"])
        raw_shares_data = api_driver.parse_api_response(urls["shares"])

        try:  # parse raw data
            self.likes_count = float(raw_likes_data["summary"]["total_count"])
        except:
            self.likes_count = 0

        try:
            self.comments_count = float(raw_comments_data["summary"]["total_count"])
        except:
            self.comments_count = 0

        try:
            self.shares_count = float(raw_shares_data["shares"]["count"])
        except:
            self.shares_count = 0

        print(
            "\tParsing post", self.message[:16], "(", self.facebook_id, ")",
            "at", self.time, "on", self.date,
            self.likes_count, "***  ",
            self.comments_count, "-.-  ",
            self.shares_count, "|-"
        )  # debug info

    def to_dict(self):
        """
        :return: dict
            Dictionary with keys (obj fields) and values (obj values)
        """

        return {
            "message": self.message,
            "time": str(self.time),
            "date": str(self.date),
            "id": self.facebook_id,
            "likes": self.likes_count,
            "comments": self.comments_count,
            "shares": self.shares_count
        }

    def to_json(self):
        """
        :return: json object
            A json representation of this object
        """

        d = self.to_dict()
        for k in d.keys():
            d[k] = str(d[k])  # convert to string to be json serializable

        return json.dumps(d)
