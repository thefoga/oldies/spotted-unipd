# !/usr/bin/python3
# coding: utf-8

# Copyright 2017 Stefano Fogarollo
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import random
import re
from collections import Counter
from random import choice


class Cup(object):
    """ A class defining a cup that will hold the words that we will pull out """

    def __init__(self):
        object.__init__(self)

        self.next_word = Counter()  # will keep track of how many times a word appears in a cup

    def add_next_word(self, word):
        """
        :param word: str
            Next word to add
        :return: void
            Used to add words to the cup and keep track of how many times we see it
        """

        self.next_word[word] += 1


class NGram(object):
    def __init__(self):
        object.__init__(self)

    @staticmethod
    def make_cups(order, string):
        """
        :param order: int
            Order of n-grams to generate
        :param string: str
            Input text
        :return: cups
            Get a string of input text and make all the unique cups with the different words in it
        """

        cups = dict()
        words = re.findall(r"([\w]+)", string)
        for i in range(len(words) - order):
            token = []
            next_word = words[i + order]
            for j in range(order):
                token.append(words[i + j])
            # create a cup if we"ve never seen this token before
            if tuple(token) not in cups:
                cups[tuple(token)] = Cup()
                cups[tuple(token)].add_next_word(next_word)
            else:
                cups[tuple(token)].add_next_word(next_word)
        return cups

    @staticmethod
    def print_cups(cups):
        """
        :param cups: Cup
            Cup to use
        :return: void
            Use this to see what cups there are and the words inside of it
        """

        for token in list(cups.keys()):
            print("key %s : nextword %s" % (token, cups[token].next_word))

    @staticmethod
    def topword(n, cup):
        """
        :param n: int
            Number of top words to take from cup
        :param cup: Cup
            Cup to use
        :return: str
            A function that picks the top words in a cup
        """

        temp = list(reversed(sorted(cup.next_word, key=cup.next_word.get)))  # reverse sort based on values
        if len(temp[:n]) > 0:
            return choice(temp[:n])  # random element from the temp list
        else:
            return ""

    def generate_from_file(self, order, filename, number_of_words):
        """
        :param order: int
            Order of n-gram
        :param filename: str
            Path to filename
        :param number_of_words: int
            Number of words to generate
        :return: str
            Generate words based on a text file
        """

        with open(filename, "r") as f:  # put file in one big string
            string = f.read()

        return self.generate_from_string(order, string, number_of_words)

    def generate_from_string(self, order, string, number_of_words):
        """
        :param order: int
            Order of n-gram
        :param string: str
            Input string
        :param number_of_words: int
            Number of words to generate
        :return: str
            Generate words based on a text file
        """

        string = re.sub("[,.?'-\'!:;]", "", string)
        cups = self.make_cups(order, string)

        first_token = choice(list(cups.keys()))  # random first token
        this_token = first_token
        generated_words = []
        generated_words += list(this_token)

        for i in range(1, number_of_words):
            # if the name of the token exists
            if this_token in cups:
                next_word = self.topword(2, cups[this_token])
            else:
                this_token = choice(list(cups.keys()))  # random token
                next_word = self.topword(2, cups[this_token])

            temp = list(this_token)
            temp.pop(0)  # remove first word in token
            temp.append(next_word)  # add new word to the end of the token
            next_token = tuple(temp)

            this_token = next_token

            generated_words += [next_word]

        print(" ".join(generated_words))


class Markov(object):
    def __init__(self, open_file):
        self.cache = {}
        self.open_file = open_file
        self.words = self.file_to_words()
        self.word_size = len(self.words)
        self.database()

    def file_to_words(self):
        data = open(self.open_file, "r").read()
        words = data.split()
        return words

    def triples(self):
        """ Generates triples from the given data string. So if our string were
                "What a lovely day", we"d generate (What, a, lovely) and then
                (a, lovely, day).
        """

        if len(self.words) < 3:
            return

        for i in range(len(self.words) - 2):
            yield (self.words[i], self.words[i + 1], self.words[i + 2])

    def database(self):
        for w1, w2, w3 in self.triples():
            key = (w1, w2)
            if key in self.cache:
                self.cache[key].append(w3)
            else:
                self.cache[key] = [w3]

    def generate_markov_text(self, size=25):
        seed = random.randint(0, self.word_size - 3)
        seed_word, next_word = self.words[seed], self.words[seed + 1]
        w1, w2 = seed_word, next_word
        gen_words = []
        for i in range(size):
            gen_words.append(w1)
            w1, w2 = w2, random.choice(self.cache[(w1, w2)])
        gen_words.append(w2)
        return " ".join(gen_words)
