# !/usr/bin/python3
# coding: utf-8

# Copyright 2017 Stefano Fogarollo
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import argparse
import os

from models import NGram, Markov

WORDS_TO_GENERATE = 100
AVAILABLE_ALGORITHMS = ["ngram", "markov"]


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage="-i <path to input file> -a <algorithm to use>")
    parser.add_argument("-i", dest="path_in", help="path to input file", required=True)
    parser.add_argument("-a", dest="algorithm", help="algorithm to use in [ngram, markov]", required=True)
    parser.add_argument("-w", dest="words", help="number of words to generate", required=False,
                        default=WORDS_TO_GENERATE)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    return str(args.path_in), str(args.algorithm), int(args.words)


def check_args(path_in, algorithm, words):
    """
    :param path_in: str
        Path to input file
    :param algorithm: str
        Algorithm to use in [ngram, markov]
    :param words: float
        Number of words to generate
    :return: bool
        True iff args are correct
    """

    assert (os.path.exists(path_in))
    assert (os.path.isfile(path_in))
    assert (algorithm in AVAILABLE_ALGORITHMS)
    assert words > 0

    return True


def main():
    path_in, algorithm, words = parse_args(create_args())
    if check_args(path_in, algorithm, words):
        if algorithm == "ngram":
            s = NGram().generate_from_file(3, path_in, words)
        elif algorithm == "markov":
            s = Markov(path_in).generate_markov_text(size=words)
        else:
            s = ""

        s = str(s).lower()
        print(s)
    else:
        print("Error while parsing args.")


if __name__ == '__main__':
    main()
