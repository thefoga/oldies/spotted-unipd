# !/usr/bin/python3
# coding: utf-8

# Copyright 2017 Stefano Fogarollo
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import argparse
import os

from .fb.models import FacebookBot  # from fb.models import FacebookBot when testing locally

AVAILABLE_OUTPUT_FORMATS = ["json", "csv"]
SPOTTED_UNIPD_PAGE_ID = "471034402958762"  # page id of Spotted Unipd Facebook page
NUMBER_OF_POSTS_TO_FETCH = 5000


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(
        usage="-t <access token> -f <format of output file [json, csv]> -o <path to output file>")
    parser.add_argument("-t", dest="token", help="access token to use Facebook API", required=True)
    parser.add_argument("-m", dest="min_post_count", help="min number of posts to fetch", required=False, default=100)
    parser.add_argument("-f", dest="format_out", help="format of output file [json, csv]", required=True)
    parser.add_argument("-o", dest="path_out", help="path to output file", required=True)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    return str(args.token), float(args.min_post_count), str(args.format_out), str(args.path_out)


def check_args(token, min_post_count, format_out, path_out):
    """
    :param token: str
        Token to use Facebook API
    :param min_post_count: float
        Minimum number of posts to fetch
    :param format_out: str
        Format of output file (json, csv)
    :param path_out: str
        File to use as output
    :return: bool
        True iff args are correct
    """

    assert (min_post_count > 1)
    assert format_out in AVAILABLE_OUTPUT_FORMATS

    out_dir = os.path.dirname(path_out)
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)  # create necessary dir for output file

    return True


def main():
    token, min_post_count, format_out, path_out = parse_args(create_args())
    if check_args(token, min_post_count, format_out, path_out):
        bot = FacebookBot(token)  # build bot with token
        if format_out == "json":
            pass
            # bot.save_json
        elif format_out == "csv":
            bot.download_page_posts(SPOTTED_UNIPD_PAGE_ID, NUMBER_OF_POSTS_TO_FETCH, path_out)
    else:
        print("Error while parsing args. Run 'pyspottedunipd -h' for help")


if __name__ == '__main__':
    main()
