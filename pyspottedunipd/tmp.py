# !/usr/bin/python3
# coding: utf-8

# Copyright 2017 Stefano Fogarollo
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Save posts for each percentile
"""
h, d = driver.parse_csv()
for p in [25, 50, 60, 70, 80, 90, 95, 99]:  # percentiles to examine
    top_posts = driver.get_top_posts(h, d, min_likes_to_be_top_posts=p)

    from hal.ml.utils.matrix import get_column_of_matrix

    msg_posts = get_column_of_matrix(h.index("message"), top_posts)  # list of post contents
    msg_posts = [str(m).strip() for m in msg_posts]
    out_file = os.path.join("/home/stefano/Coding/Python/projects/spotted-unipd/data/text/",
                            str(p) + "_percentile_posts.txt")
    with open(out_file, "w") as o:
        o.writelines(msg_posts)

"""
Compute top words for each file
"""
INPUT_FOLDER = "/home/stefano/Coding/Python/projects/spotted-unipd/data/text/"
PERCENTILES = [0, 25, 50, 60, 70, 80, 90, 95, 99]
for p in PERCENTILES:
    f = os.path.join(INPUT_FOLDER, str(p) + "_percentile_posts.txt")
    top_words = get_top_words_in_file(f, WORDS_BLACKLIST_FIlE)
    top_words = [" ".join([w, str(q)]) + "\n" for w, q in top_words]

    out_file = os.path.join(INPUT_FOLDER, str(p) + "_percentile_top_words.txt")
    with open(out_file, "w") as o:
        o.writelines(top_words)
