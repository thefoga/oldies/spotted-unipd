# !/usr/bin/python3
# coding: utf-8

# Copyright 2017 Stefano Fogarollo
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import datetime

import matplotlib.pyplot as plt
import numpy as np
from hal.charts.bar import create_bar_chart, create_multiple_bar_chart
from hal.ml.analysis import correlation
from hal.ml.data.parser import parse_csv_file
from hal.ml.utils import matrix as m_utils
from hal.ml.utils.matrix import add_columns_to_matrix
from hal.strings.utils import get_average_length_of_word
from hal.time.utils import get_seconds, MONTHS
from sklearn import linear_model, cluster, feature_selection

from pyspottedunipd.utils import save_matrix_to_csv


# include if error in projection from mpl_toolkits.mplot3d.axes3d import Axes3D


class FacebookPostsDataFilter(object):
    """ Parses and fixes raw data """

    def __init__(self, dataset_file):
        """
        :param dataset_file: str
            Path to folder with data to analyse
        """

        object.__init__(self)

        self.dataset_file = dataset_file

    def parse_csv(self):
        """
        :return: tuple [], [] of []
            Headers of csv file and data
        """

        return parse_csv_file(self.dataset_file)

    def write_csv(self, headers, data, out_file):
        """
        :param headers: [] of str
            Names of columns
        :param data: [] of []
            Actual data
        :param out_file: str
            Path to output file
        :return: void
            Saves data in csv file
        """

        save_matrix_to_csv(headers, data, out_file)

    @staticmethod
    def convert_time_columns(headers, headers_to_convert, data):
        """
        :param headers: [] of str
            Column names of data
        :param headers_to_convert: [] of str
            Column names of data to convert from time format to float
        :param data: [] of []
            Raw data
        :return: [] of []
            Input data but with converted time columns
        """

        d = data
        for i in range(len(headers)):
            if headers[i] in headers_to_convert:  # this columns id to be converted
                for row in range(len(data)):  # convert all rows of this column
                    d[row][i] = get_seconds(data[row][i])
        return d

    @staticmethod
    def add_time_columns(headers, data, date_header, time_header):
        """
        :param headers: [] of str
            Names of columns
        :param data: [] of []
            Actual data
        :param date_header: str
            Name of column containing date data
        :param time_header: str
            Name of column containing time data
        :return: tuple [] of str, matrix ([] of [])
            Data with edited headers and added useful time columns.
            Adds hour, minute, week day, month, year headers
        """

        # 1: deal with date columns
        new_date_columns = []  # list with dates data
        column_index = headers.index(date_header)
        date_column = []  # column with date data
        for row in range(len(data)):
            date_column.append(data[row][column_index])

        for d in date_column:
            date = datetime.datetime.strptime(d, "%Y-%m-%d")
            new_date_columns.append([date.year, date.month, date.day])  # get date components and add to list
        new_headers, new_data = add_columns_to_matrix(headers, data, ["year", "month", "day"], new_date_columns)

        # 2: deal with time columns
        new_time_columns = []  # list with times data
        column_index = headers.index(time_header)
        time_column = []  # column with time data
        for row in range(len(data)):
            time_column.append(data[row][column_index])

        for d in time_column:
            time = datetime.datetime.strptime(d, "%H:%M:%S")
            new_time_columns.append([time.hour, time.minute, time.second])  # get time components and add to list

        return add_columns_to_matrix(
            new_headers,
            new_data,
            ["hour", "minute", "second"],
            new_time_columns
        )

    @staticmethod
    def add_msg_stats_columns(headers, data, message_column):
        """
        :param headers: [] of str
            Names of columns
        :param data: [] of []
            Actual data
        :param message_column: str
            Name of column with message post
        :return: tuple [] of str, matrix ([] of [])
            Data with edited headers and added useful time columns.
            Adds message length, number of words, average word length column
        """

        new_msg_columns = []  # list with dates data
        column_index = headers.index(message_column)
        msg_column = []  # column with message data
        for row in range(len(data)):
            msg_column.append(data[row][column_index])

        for p in msg_column:
            words = p.split()  # collection of words
            words_longer_than_3 = [w for w in words if len(w) > 3]
            words_longer_than_5 = [w for w in words if len(w) > 5]
            words_longer_than_7 = [w for w in words if len(w) > 7]
            words_longer_than_9 = [w for w in words if len(w) > 9]
            words_longer_than_11 = [w for w in words if len(w) > 11]

            new_msg_columns.append([  # for each group compute length and average
                len(words),
                get_average_length_of_word(words),
                len(words_longer_than_3),
                get_average_length_of_word(words_longer_than_3),
                len(words_longer_than_5),
                get_average_length_of_word(words_longer_than_5),
                len(words_longer_than_7),
                get_average_length_of_word(words_longer_than_7),
                len(words_longer_than_9),
                get_average_length_of_word(words_longer_than_9),
                len(words_longer_than_11),
                get_average_length_of_word(words_longer_than_11)
            ])  # get date components and add to list

        return add_columns_to_matrix(
            headers,
            data,
            [
                "# words",
                "avg length of words",
                "# words longer than 3",
                "avg length of words longer than 3",
                "# words longer than 5",
                "avg length of words longer than 5",
                "# words longer than 7",
                "avg length of words longer than 7",
                "# words longer than 9",
                "avg length of words longer than 9",
                "# words longer than 11",
                "avg length of words longer than 11"
            ],
            new_msg_columns
        )


class StatsAnalysis(FacebookPostsDataFilter):
    """ Computes correlation of data and statistical analysis """

    def __init__(self, dataset_file):
        """
        :param dataset_file: str
            Path to folder with data to analyse
        """

        FacebookPostsDataFilter.__init__(self, dataset_file)

    def show_correlation_matrix(self, title_image, headers_to_analyze):
        """
        :param title_image: str
            Title of output image
        :param headers_to_analyze: [] of str
            Compute correlation matrix of only these headers
        :return: void
            Shows correlation matrix of data of files in folder
        """

        print("Computing correlation matrix of file ", str(self.dataset_file))
        headers, data = self.parse_csv()  # parse raw data
        correlation.show_correlation_matrix_of_columns(
            title_image,
            headers_to_analyze,  # headers to test
            headers,
            data
        )

    @staticmethod
    def get_top_posts(headers, data, likes_header="likes", min_likes_to_be_top_posts=90):
        """
        :param headers: [] of str
            Names of columns
        :param data: [] of []
            Actual data
        :param likes_header: str
            Column name of likes count
        :param min_likes_to_be_top_posts: float in [0, 1]
            Percentile for a post to be considered top post
        :return: [] of []
            List of top posts (#likes >= x * max_likes)
        """

        likes_count_column = headers.index(likes_header)  # column index with likes data
        likes_count = np.array(m_utils.get_column_of_matrix(likes_count_column, data)).astype(np.float)
        min_likes_to_be_top_post = np.percentile(likes_count, min_likes_to_be_top_posts)  # get percentile
        top_posts = []  # list of top posts
        for row in data:
            if np.float(row[likes_count_column]) > min_likes_to_be_top_post:  # if post is top
                top_posts.append(row)

        return top_posts


class TimeAnalysis(StatsAnalysis):
    """ Analysis on timing of posts """

    HEADERS_TO_ANALYZE = [
        # "message",
        "comments",
        # "date",
        # "time",
        "likes",
        "shares",
        # "id",
        # "year",
        "month",
        "day",
        "hour",
        "minute",
        # "second",
        "# words",
        "avg length of words",
        "# words longer than 3",
        "avg length of words longer than 3",
        "# words longer than 5",
        "avg length of words longer than 5",
        "# words longer than 7",
        "avg length of words longer than 7",
        "# words longer than 9",
        "avg length of words longer than 9",
        "# words longer than 11",
        "avg length of words longer than 11"
    ]  # get correlation analysis only for these columns
    TIME_HEADERS_TO_CONVERT = [
        # "time"
    ]  # columns of data file to convert from time format to float

    def __init__(self, dataset_file):
        """
        :param dataset_file: str
            Path to folder with data to analyse
        """

        StatsAnalysis.__init__(self, dataset_file)

    def parse_csv(self):
        """
        :return: tuple [], [] of []
            Headers of csv file and data
        """

        headers, data = super(StatsAnalysis, self).parse_csv()
        data = self.convert_time_columns(headers, self.TIME_HEADERS_TO_CONVERT, data)
        return headers, data

    def months_clusters_of_top_posts(self):
        """
        :return: void
            Shows plot with x: months, y number of most liked posts in that month
        """

        print("Time-clustering data file", self.dataset_file)
        headers, raw_data = self.parse_csv()  # get columns names and raw data

        y_values = []
        x_labels = []
        percentiles_to_analyze = [25, 50, 75, 95]  # get posts counts for these percentiles
        for p in percentiles_to_analyze:
            posts = self.get_top_posts(headers, raw_data, min_likes_to_be_top_posts=p)
            y_values.append(
                [
                    m_utils.get_column_of_matrix(
                        headers.index("month"),
                        posts
                    ).count(str(m)) for m in MONTHS.keys()  # count how many posts have this month as attribute
                    ]
            )  # count of posts with this percentile
            x_labels.append(
                [
                    "Number of posts liked > " + str(p) + "%"
                ]
            )  # label

        create_multiple_bar_chart(
            "Number of posts for each month",
            [v for v in MONTHS.values()],
            y_values,
            x_labels
        )  # create chart
        plt.show()  # show chart

    def hours_clusters_of_top_posts(self):
        """
        :return: void
            Shows plot with x: hours, y number of most liked posts in that month
        """

        print("Time-clustering data file", self.dataset_file)
        headers, raw_data = self.parse_csv()  # get columns names and raw data

        y_values = []
        x_labels = []
        percentiles_to_analyze = [25, 50, 75, 95]  # get posts counts for these percentiles
        for p in percentiles_to_analyze:
            posts = self.get_top_posts(headers, raw_data, min_likes_to_be_top_posts=p)
            y_values.append(
                [
                    m_utils.get_column_of_matrix(
                        headers.index("hour"),
                        posts
                    ).count(str(h)) for h in range(24)  # count how many posts have this month as attribute
                    ]
            )  # count of posts with this percentile
            x_labels.append(
                [
                    "Number of posts liked > " + str(p) + "%"
                ]
            )  # label

        create_multiple_bar_chart(
            "Number of posts for each hour",
            [str(h) for h in range(24)],
            y_values,
            x_labels
        )  # create chart
        plt.show()  # show chart


class MLAnalysis(StatsAnalysis):
    """ Carries out popular machine-learning tasks on Facebook posts data """

    HEADERS_TO_ANALYZE = [
        # "message",
        "comments",
        # "date",
        # "time",
        "likes",
        "shares",
        # "id",
        # "year",
        "month",
        "day",
        "hour",
        "minute",
        # "second",
        "# words",
        "avg length of words",
        "# words longer than 3",
        "avg length of words longer than 3",
        "# words longer than 5",
        "avg length of words longer than 5",
        "# words longer than 7",
        "avg length of words longer than 7",
        "# words longer than 9",
        "avg length of words longer than 9",
        "# words longer than 11",
        "avg length of words longer than 11"
    ]  # get correlation analysis only for these columns
    TIME_HEADERS_TO_CONVERT = [
        # "time"
    ]  # columns of data file to convert from time format to float

    def __init__(self, dataset_file):
        """
        :param dataset_file: str
            Path to folder with data to analyse
        """

        StatsAnalysis.__init__(self, dataset_file)

    def parse_csv(self):
        """
        :return: tuple [], [] of []
            Headers of csv file and data
        """

        headers, data = super(StatsAnalysis, self).parse_csv()
        data = self.convert_time_columns(headers, self.TIME_HEADERS_TO_CONVERT, data)
        return headers, data

    def show_correlation_matrix_of_data(self):
        """
        :return: void
            Shows correlation matrix of data of files in folder
        """

        self.show_correlation_matrix("Spotted Unipd correlation data of ~5000 posts", self.HEADERS_TO_ANALYZE)

    def predict_feature(self, feature):
        """
        :param feature: str
            Name of feature (column name) to predict
        :return: void
            Predicts feature with linear regression
        """

        print("Predicting ", feature, "with data from file", self.dataset_file)
        headers, raw_data = self.parse_csv()  # get columns names and raw data
        clf = linear_model.LinearRegression()  # model to fit data
        x_matrix_features = self.HEADERS_TO_ANALYZE.copy()
        x_matrix_features.remove(feature)  # do NOT include feature to predict in input matrix
        x_data = m_utils.get_subset_of_matrix(x_matrix_features, headers, raw_data)  # input matrix
        y_data = m_utils.get_subset_of_matrix([feature], headers, raw_data)  # output matrix
        clf.fit(x_data, y_data)

        coefficients = {}  # dict feature -> coefficient
        for i in range(len(x_matrix_features)):
            coefficients[x_matrix_features[i]] = clf.coef_[0][i]

        create_bar_chart(
            "Linear fit of " + feature + " based on ~5000 posts",
            [k for k in coefficients.keys()],
            coefficients.values(),
            "Coefficient"
        )
        plt.show()

    def cluster_analyze(self, n_clusters=6):
        """
        :param n_clusters: int
            Number of clusters
        :return: void
            Computes cluster analysis: see how each cluster is made up.
        """

        print("Clustering file", self.dataset_file)
        headers, raw_data = self.parse_csv()  # get columns names and raw data
        x_data = m_utils.get_subset_of_matrix(self.HEADERS_TO_ANALYZE, headers, raw_data)  # input matrix
        kmeans = cluster.KMeans(n_clusters=n_clusters, random_state=0).fit(x_data)
        print("Clusters", kmeans.labels_)

        headers_to_plot = [  # TODO edit headers
            "likes",
            "shares",
            "# words",
            "# words longer than 11",
        ]  # get headers to add to chart
        centroids = kmeans.cluster_centers_  # centroids of each cluster
        cluster_centers = []  # list of centers of each cluster
        for i in range(n_clusters):
            cl_center = {}  # coordinates of center of each cluster
            for j in range(len(headers_to_plot)):
                cl_center[headers_to_plot[j]] = centroids[i][
                    self.HEADERS_TO_ANALYZE.index(headers_to_plot[j])]  # j-coordinate of i-th cluster
            cluster_centers.append(cl_center)

        vals_headers = [
            [c[h] for c in cluster_centers] for h in headers_to_plot
            ]  # get center of each cluster
        clusters = [str(i) for i in range(n_clusters)]  # get list of clusters (x values)

        create_multiple_bar_chart(
            "Cluster group",
            clusters,
            vals_headers,
            headers_to_plot
        )  # create chart
        plt.show()  # show bar chart

    def cluster_3d_plot(self, labels, n_clusters=6):
        """
        :param labels: [] of str (len = 3)
            Features to cluster data. Each item must be in the csv data file. Each label is one of x, y, z axis
        :param n_clusters: int
            Number of clusters
        :return: void
            Plots 3D chart with clusters based on selected features
        """

        print("Clustering file", self.dataset_file)
        headers, raw_data = self.parse_csv()  # get columns names and raw data
        x_data = m_utils.get_subset_of_matrix(self.HEADERS_TO_ANALYZE, headers, raw_data)  # input matrix
        kmeans = cluster.KMeans(n_clusters=n_clusters, random_state=0).fit(x_data)

        fig = plt.figure(figsize=(4, 3))  # create 3D plot
        ax = fig.add_subplot(111, projection="3d")
        ax.scatter(
            x_data[:, self.HEADERS_TO_ANALYZE.index(labels[0])],  # get values of given labels
            x_data[:, self.HEADERS_TO_ANALYZE.index(labels[1])],
            x_data[:, self.HEADERS_TO_ANALYZE.index(labels[2])],
            c=kmeans.labels_.astype(np.float)
        )  # plot 3D data points

        centroids = kmeans.cluster_centers_
        cluster_centers = []  # list of centers of each cluster
        for i in range(n_clusters):
            cl_center = {
                "x": centroids[i][self.HEADERS_TO_ANALYZE.index(labels[0])],  # x-coordinate of i-th cluster
                "y": centroids[i][self.HEADERS_TO_ANALYZE.index(labels[1])],  # y-coordinate of i-th cluster
                "z": centroids[i][self.HEADERS_TO_ANALYZE.index(labels[2])]  # z-coordinate of i-th cluster
            }  # x, y, z of center of first cluster -> find x, y, z of each label
            cluster_centers.append(cl_center)

        ax.scatter(
            [c["x"] for c in cluster_centers],  # x positions of centers of all clusters
            [c["y"] for c in cluster_centers],  # y positions of centers of all clusters
            [c["z"] for c in cluster_centers],  # z positions of centers of all clusters
            marker='o',
            s=1000,
            linewidth=5,
            color='w'
        )  # plot centroids

        ax.set_xlabel(labels[0])  # set labels
        ax.set_ylabel(labels[1])
        ax.set_zlabel(labels[2])

        plt.title(str(n_clusters) + "-clustering ~5000 posts")
        plt.show()

    def select_k_best(self, feature, k=5):
        """
        :param feature: str
            Name of feature (column name) to predict
        :param k: int
            Number of features to select
        :return: void
            Selects the best features to predict feature
        """

        print("Selecting k best features of data file", self.dataset_file)
        headers, raw_data = self.parse_csv()  # get columns names and raw data
        sel = feature_selection.SelectKBest(feature_selection.f_regression, k=k)  # model to select data
        x_matrix_features = self.HEADERS_TO_ANALYZE.copy()  # not edit main list of headers
        x_matrix_features.remove(feature)  # do NOT include feature to predict in input matrix
        x_data = m_utils.get_subset_of_matrix(x_matrix_features, headers, raw_data)  # input matrix
        y_data = m_utils.get_subset_of_matrix([feature], headers, raw_data)  # output matrix
        sel.fit(x_data, y_data)  # fit

        top_k_features_indices = np.array(sel.scores_).argsort()[-k:][::-1]  # indices of top k features
        top_k_features = [x_matrix_features[i] for i in top_k_features_indices]  # names of top k features
        top_k_features_scores = [sel.scores_[i] for i in top_k_features_indices]  # scores of top k features

        create_bar_chart("Most " + str(k) + " correlated features with " + feature, top_k_features,
                         top_k_features_scores, "score")
        plt.show()
