# !/usr/bin/python3
# coding: utf-8

# Copyright 2017 Stefano Fogarollo
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import argparse
import os
import time

from models import MLAnalysis, TimeAnalysis, FacebookPostsDataFilter


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage="-i <path to input file>")
    parser.add_argument("-i", dest="path_in", help="path to input file", required=True)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    return str(args.path_in)


def check_args(path_in):
    """
    :param path_in: str
        File to use as input
    :return: bool
        True iff args are correct
    """

    assert (os.path.exists(path_in))
    return True


def add_time_msg_stats(path_in):
    """
    :param path_in: str
        File to use as input
    :return: void
        Extract time and message statistical features from data (in input file) and saves new data in in new file
    """

    path_out = os.path.join(os.path.dirname(path_in), "out_time_msg_" + str(int(time.time())) + ".csv")
    driver = FacebookPostsDataFilter(path_in)
    h, d = driver.parse_csv()  # get data
    h, d = driver.add_time_columns(h, d, "date", "time")  # add time features
    h, d = driver.add_msg_stats_columns(h, d, "message")  # add msg features
    driver.write_csv(h, d, path_out)


def ml_analyze(path_in):
    """
    :param path_in: str
        File to use as input
    :return: void
        Carries out sample machine learning analytics
    """

    driver = MLAnalysis(path_in)
    driver.show_correlation_matrix_of_data()  # correlation matrix
    driver.predict_feature("likes")  # linear fits
    driver.predict_feature("shares")
    driver.predict_feature("comments")
    driver.cluster_3d_plot(["likes", "comments", "shares"], n_clusters=5)  # clustering
    driver.cluster_analyze(n_clusters=3)
    driver.select_k_best("likes")  # k-best features
    driver.select_k_best("shares")
    driver.select_k_best("comments")


def time_analyze(path_in):
    """
    :param path_in: str
        File to use as input
    :return: void
        Carries out sample timing analytics
    """

    driver = TimeAnalysis(path_in)
    driver.hours_clusters_of_top_posts()
    driver.months_clusters_of_top_posts()


def main():
    path_in = parse_args(create_args())
    if check_args(path_in):
        ml_analyze(path_in)
        time_analyze(path_in)
    else:
        print("Error while parsing args.")


if __name__ == '__main__':
    main()
