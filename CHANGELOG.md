# Change Log
All notable changes to this project will be documented in this file.

### TODO
- test suite

## 0.0.9 - 2017-04-20

### Added
- get_top_words_in_file
- text analysis
- readme top 30 words

## 0.0.8 - 2017-04-20

### Added
- get_top_posts
- likes count per month with image
- likes count per hour with image
- TimeAnalysis class

## 0.0.7 - 2017-04-19

### Added
- ml analysis
- sample ml analysis images
- images in REDME

### Fixed
- cluster analysis

### Changed
- charts description

## 0.0.6 - 2017-04-19

### Added
- ngrams cli
- msg stats in DataFilter

### Removed
- raw ngrams and fb data

### Fixed
- args in analysis.cli

## 0.0.5 - 2017-04-18

### Added
- ngrams sample script
- sample ngrams in README
- markov ngrams

## 0.0.4 - 2017-04-18

### Added
- guidelines and samples in README
- cli for ml-analysis
- MLAnalysis.add_time_columns
- 5000 posts data

## 0.0.3 - 2017-04-18

### Added
- fetching also shares count
- MLAnalysis and FacebookPostsDataFilter
- sample csv output

### Removed
- useless parse_hh_mm_ss in cli.py

### Fixed
- api url
- fetching exact post number in get_latest_posts_of_page
- handling key errors in API response

## 0.0.2 - 2017-04-18

### Added
- get_api_driver
- FacebookPost, FacebookBot, FacebookGraphApi
- FacebookBot.get_raw_posts_of_page
- docs
- README docs and badges

## 0.0.1 - 2017-04-17

### Added
- setup
- fb, spotted_unipd python packages
- sample usage in README

### Fixed
- setup.py package

## 0.0 - 2017-04-17

### Added
- repository
- README
- folder struct
