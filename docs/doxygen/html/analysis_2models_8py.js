var analysis_2models_8py =
[
    [ "FacebookPostsDataFilter", "classpyspottedunipd_1_1analysis_1_1models_1_1_facebook_posts_data_filter.html", "classpyspottedunipd_1_1analysis_1_1models_1_1_facebook_posts_data_filter" ],
    [ "StatsAnalysis", "classpyspottedunipd_1_1analysis_1_1models_1_1_stats_analysis.html", "classpyspottedunipd_1_1analysis_1_1models_1_1_stats_analysis" ],
    [ "MLAnalysis", "classpyspottedunipd_1_1analysis_1_1models_1_1_m_l_analysis.html", "classpyspottedunipd_1_1analysis_1_1models_1_1_m_l_analysis" ],
    [ "add_columns_to_matrix", "analysis_2models_8py.html#aacaed9cf9080226fd0c70c6a310df4ab", null ],
    [ "get_average_length_of_word", "analysis_2models_8py.html#af22f0a07610463f6ebc3ad838163c38c", null ],
    [ "remove_column_from_matrix", "analysis_2models_8py.html#a97bf9c28dc7f888aeb5d282f5d7e539e", null ]
];