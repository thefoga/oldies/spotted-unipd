var hierarchy =
[
    [ "object", null, [
      [ "pyspottedunipd.analysis.models.FacebookPostsDataFilter", "classpyspottedunipd_1_1analysis_1_1models_1_1_facebook_posts_data_filter.html", [
        [ "pyspottedunipd.analysis.models.StatsAnalysis", "classpyspottedunipd_1_1analysis_1_1models_1_1_stats_analysis.html", [
          [ "pyspottedunipd.analysis.models.MLAnalysis", "classpyspottedunipd_1_1analysis_1_1models_1_1_m_l_analysis.html", null ],
          [ "pyspottedunipd.analysis.models.TimeAnalysis", "classpyspottedunipd_1_1analysis_1_1models_1_1_time_analysis.html", null ]
        ] ]
      ] ],
      [ "pyspottedunipd.fb.models.FacebookBot", "classpyspottedunipd_1_1fb_1_1models_1_1_facebook_bot.html", null ],
      [ "pyspottedunipd.fb.models.FacebookGraphApi", "classpyspottedunipd_1_1fb_1_1models_1_1_facebook_graph_api.html", null ],
      [ "pyspottedunipd.fb.models.FacebookPost", "classpyspottedunipd_1_1fb_1_1models_1_1_facebook_post.html", null ],
      [ "pyspottedunipd.ngrams.models.Cup", "classpyspottedunipd_1_1ngrams_1_1models_1_1_cup.html", null ],
      [ "pyspottedunipd.ngrams.models.Markov", "classpyspottedunipd_1_1ngrams_1_1models_1_1_markov.html", null ],
      [ "pyspottedunipd.ngrams.models.NGram", "classpyspottedunipd_1_1ngrams_1_1models_1_1_n_gram.html", null ]
    ] ]
];