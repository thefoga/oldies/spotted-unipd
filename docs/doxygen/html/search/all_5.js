var searchData=
[
  ['generate_5ffrom_5ffile',['generate_from_file',['../classpyspottedunipd_1_1ngrams_1_1models_1_1_n_gram.html#a6fb8e14cc45dc03c7cd250a7f6804c70',1,'pyspottedunipd::ngrams::models::NGram']]],
  ['generate_5ffrom_5fstring',['generate_from_string',['../classpyspottedunipd_1_1ngrams_1_1models_1_1_n_gram.html#adde2b71326b9837fffbf642057167fab',1,'pyspottedunipd::ngrams::models::NGram']]],
  ['generate_5fmarkov_5ftext',['generate_markov_text',['../classpyspottedunipd_1_1ngrams_1_1models_1_1_markov.html#a19bccbd777575d1c3e036d4609be0355',1,'pyspottedunipd::ngrams::models::Markov']]],
  ['get_5fapi_5fdriver',['get_api_driver',['../classpyspottedunipd_1_1fb_1_1models_1_1_facebook_graph_api.html#a32ae10d5a9d99a53f4b7b5da877d0446',1,'pyspottedunipd::fb::models::FacebookGraphApi']]],
  ['get_5fapi_5furl',['get_api_url',['../classpyspottedunipd_1_1fb_1_1models_1_1_facebook_graph_api.html#a9901b00a439222ccf96fbf91d27d4a33',1,'pyspottedunipd::fb::models::FacebookGraphApi']]],
  ['get_5flatest_5fposts_5fof_5fpage',['get_latest_posts_of_page',['../classpyspottedunipd_1_1fb_1_1models_1_1_facebook_bot.html#a84bbc98341ead3cb11aebf0e0b5b32a5',1,'pyspottedunipd::fb::models::FacebookBot']]],
  ['get_5fpage_5fposts_5furl',['get_page_posts_url',['../classpyspottedunipd_1_1fb_1_1models_1_1_facebook_graph_api.html#ab365864c35ba403da5cef6b985da164b',1,'pyspottedunipd::fb::models::FacebookGraphApi']]],
  ['get_5fpost_5fsummary_5furls',['get_post_summary_urls',['../classpyspottedunipd_1_1fb_1_1models_1_1_facebook_graph_api.html#aa3458c477bb0c0be988eb3a60a2bac88',1,'pyspottedunipd::fb::models::FacebookGraphApi']]],
  ['get_5ftop_5fposts',['get_top_posts',['../classpyspottedunipd_1_1analysis_1_1models_1_1_stats_analysis.html#adf5b8a2bcb4064edd61e8836125ad622',1,'pyspottedunipd::analysis::models::StatsAnalysis']]],
  ['get_5ftop_5fwords',['get_top_words',['../namespacepyspottedunipd_1_1text_1_1cli.html#afabc2bcd2cf6873c7ca491f93af53c73',1,'pyspottedunipd::text::cli']]],
  ['get_5ftop_5fwords_5fin_5ffile',['get_top_words_in_file',['../namespacepyspottedunipd_1_1text_1_1cli.html#acca7efbea223e97a06e0d8f12b68ed5f',1,'pyspottedunipd::text::cli']]],
  ['get_5ftotals',['get_totals',['../classpyspottedunipd_1_1fb_1_1models_1_1_facebook_post.html#a437047c61683ad307ef6c62014910f3f',1,'pyspottedunipd::fb::models::FacebookPost']]],
  ['get_5fwords',['get_words',['../namespacepyspottedunipd_1_1text_1_1cli.html#a6c02ddc4e9df3619bb465489928c8ada',1,'pyspottedunipd::text::cli']]],
  ['get_5fwords_5fcount',['get_words_count',['../namespacepyspottedunipd_1_1text_1_1cli.html#a24afe22f8aef59c504d5dbfbad61ace1',1,'pyspottedunipd::text::cli']]],
  ['get_5fwords_5fin_5ffile',['get_words_in_file',['../namespacepyspottedunipd_1_1text_1_1cli.html#afe221348bdf05b1fa87890755f95017b',1,'pyspottedunipd::text::cli']]],
  ['graph',['graph',['../classpyspottedunipd_1_1fb_1_1models_1_1_facebook_bot.html#a3857b4ed1eab23cf1005665d92cf0448',1,'pyspottedunipd::fb::models::FacebookBot']]]
];
