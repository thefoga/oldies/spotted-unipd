var searchData=
[
  ['add_5fmsg_5fstats_5fcolumns',['add_msg_stats_columns',['../classpyspottedunipd_1_1analysis_1_1models_1_1_facebook_posts_data_filter.html#a39d5f5617721928dc6298443f2596e58',1,'pyspottedunipd::analysis::models::FacebookPostsDataFilter']]],
  ['add_5fnext_5fword',['add_next_word',['../classpyspottedunipd_1_1ngrams_1_1models_1_1_cup.html#a2d8043239f6a2be5041b59c13b010adb',1,'pyspottedunipd::ngrams::models::Cup']]],
  ['add_5ftime_5fcolumns',['add_time_columns',['../classpyspottedunipd_1_1analysis_1_1models_1_1_facebook_posts_data_filter.html#a1a78fcf3b4ce5299afdf160b79636a35',1,'pyspottedunipd::analysis::models::FacebookPostsDataFilter']]],
  ['add_5ftime_5fmsg_5fstats',['add_time_msg_stats',['../namespacepyspottedunipd_1_1analysis_1_1cli.html#a489ab36cfff74eaaf3a2ad2ddd8ad5df',1,'pyspottedunipd::analysis::cli']]],
  ['api',['api',['../classpyspottedunipd_1_1fb_1_1models_1_1_facebook_bot.html#a723a55e3dedece9b7800cd0aaf12acd7',1,'pyspottedunipd::fb::models::FacebookBot']]],
  ['available_5falgorithms',['AVAILABLE_ALGORITHMS',['../namespacepyspottedunipd_1_1ngrams_1_1cli.html#af60fcd354c98b1e8a04128927c492fb3',1,'pyspottedunipd::ngrams::cli']]],
  ['available_5foutput_5fformats',['AVAILABLE_OUTPUT_FORMATS',['../namespacepyspottedunipd_1_1cli.html#af1835d3a86820dbe36529b5de3dc5a5b',1,'pyspottedunipd::cli']]]
];
