var searchData=
[
  ['save_5fdicts_5fto_5fcsv',['save_dicts_to_csv',['../namespacepyspottedunipd_1_1utils.html#a5df602a125f1606e9da242fcfce67ee2',1,'pyspottedunipd::utils']]],
  ['save_5fmatrix_5fto_5fcsv',['save_matrix_to_csv',['../namespacepyspottedunipd_1_1utils.html#a7e92e6af5f5db70bccb8d69f191f27a6',1,'pyspottedunipd::utils']]],
  ['select_5fk_5fbest',['select_k_best',['../classpyspottedunipd_1_1analysis_1_1models_1_1_m_l_analysis.html#adf85b34681dd486c6cd9b73f822cdcdb',1,'pyspottedunipd::analysis::models::MLAnalysis']]],
  ['shares_5fcount',['shares_count',['../classpyspottedunipd_1_1fb_1_1models_1_1_facebook_post.html#af8bd89a0ad99c511c7aa8aa87da852dd',1,'pyspottedunipd::fb::models::FacebookPost']]],
  ['show_5fcorrelation_5fmatrix',['show_correlation_matrix',['../classpyspottedunipd_1_1analysis_1_1models_1_1_stats_analysis.html#a496abc3705fe7bf77f582593185554aa',1,'pyspottedunipd::analysis::models::StatsAnalysis']]],
  ['show_5fcorrelation_5fmatrix_5fof_5fdata',['show_correlation_matrix_of_data',['../classpyspottedunipd_1_1analysis_1_1models_1_1_m_l_analysis.html#a1db6345c9add69921d00c043db5da1f7',1,'pyspottedunipd::analysis::models::MLAnalysis']]],
  ['spotted_5funipd_5fpage_5fid',['SPOTTED_UNIPD_PAGE_ID',['../namespacepyspottedunipd_1_1cli.html#affdb4554353d0a89a69903d4ecd3ac73',1,'pyspottedunipd::cli']]],
  ['statsanalysis',['StatsAnalysis',['../classpyspottedunipd_1_1analysis_1_1models_1_1_stats_analysis.html',1,'pyspottedunipd::analysis::models']]]
];
